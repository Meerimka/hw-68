import React, {Component} from 'react';
import './task.css';
import {connect} from 'react-redux';
import {fetchTasks} from "../../store/actions";

class Tasks extends Component {

    componentDidMount() {
        this.props.getTasks().then(() => {
            console.log(this.props.tasks);
        });
    }
    render() {
      return (
          <div className="Task">
              <p>{this.props.task}</p>
              <button>**</button>
              {/*<button onClick={this.props.remove}>X</button>*/}
          </div>
      )
  }
};

const mapStateToProps = (state) => {
    return {
        tasks: state.tasks
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        getTasks: () => dispatch(fetchTasks())
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Tasks);