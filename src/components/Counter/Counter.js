import React, {Component} from 'react';
import {connect} from 'react-redux';

import './Counter.css';
import {increaseByFiveCounter, decrementCounter, fetchCounter, incrementCounter, subtractCounter} from "../../store/actions";
import Spinner from "../../components/Spinner/Spinner";

class Counter extends Component {
    componentDidMount() {
        this.props.loadData();

        console.log(this.props.ctr);
    }

    render() {
        return (
            <div className='Counter'>
                <h1>{this.props.ctr}</h1>
                {/*{this.props.loading ? <Spinner/> : <h1>{this.props.ctr}</h1>}*/}
                <button onClick={this.props.increaseCounter}>Increase</button>
                <button onClick={this.props.decreaseCounter}>Decrease</button>
                <button onClick={this.props.increaseByFiveCounter}>Increase by 5</button>
                <button onClick={this.props.subtractCounter}>Decrease by 5</button>
            </div>
        );
    }
}
const mapStateToProps = state => {
    return {
        ctr: state.counter,
        loading: state.loading
    };
};
const mapDispatchToProps = dispatch => {
    return {
        increaseCounter: () => dispatch(incrementCounter()),
        decreaseCounter: () => dispatch(decrementCounter()),
        increaseByFiveCounter: (amount) => dispatch(increaseByFiveCounter(5)),
        subtractCounter: (amount) => dispatch(subtractCounter(5)),
        loadData: () => dispatch(fetchCounter()),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Counter);