import React, {Component} from 'react';
import {connect} from "react-redux";
import {addTask} from "../../store/actions"
import './TaskForm.css';

class TaskForm extends Component {
    state = {
        currentId: 3,
        tasks: [
            {task: 'buy Milk',  id: 1},
            {task: 'go to shop', id: 2},
            {task: 'call to mummy',  id: 3}
        ],
        showTasks: false,
        value: ''
    };

    addHandler = () => {
        const newTask = {task: this.state.value, id: this.state.currentId + 1};
        let tasks = [...this.state.tasks, newTask];
        this.setState({tasks, currentId: this.state.currentId + 1, value: ""})
    };

    changeHandler = event => {
        this.setState({value: event.target.value})
    };

    removeHandler = id =>{
        const tasks = [...this.state.tasks];
        const taskId = tasks.findIndex(task => task.id === id);
        tasks.splice(taskId, 1);
        this.setState({tasks});
    };
    render() {
        return (
            <div className="AddTask">
              <h1>{this.state.title}</h1>
              <p>
              <input type="text" value={this.state.value} onChange={(e) => this.changeHandler(e)}/>
              <button onClick={this.state.addHandler}>Add</button>
              </p>
            </div>
        );
    }
}

const mapStateToProps =(state) =>{
    return{
        tasks: state.tasks
    }
};

const mapDispatchToProps =(dispatch)=>{
    return{
        addTask: () => dispatch(addTask())
    }
}

export default connect (mapStateToProps,mapDispatchToProps)(TaskForm);