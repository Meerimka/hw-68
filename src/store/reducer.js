import {
    INCREASE_BY_FIVE,
    DECREMENT,
    INCREMENT,
    SUBTRACT,
    ADD,
    FETCH_COUNTER_ERROR,
    FETCH_COUNTER_REQUEST,
    FETCH_COUNTER_SUCCESS,
    REMOVE,
    FETCH_TASKS
} from "./actions";

const initialState={
    currentId: 3,
    tasks: [
        {task: 'buy Milk',  id: 1},
        {task: 'go to shop', id: 2},
        {task: 'call to mummy',  id: 3}
    ],
    showTasks: false,
    value: '',
    counter: 0,
    loading: false
};

const reducer = (state= initialState, action) => {
    switch (action.type) {
        case FETCH_TASKS:
            return {...state, tasks: action.data};
        case INCREMENT:
            return {
                ...state,
                counter: state.counter + 1
            };
        case DECREMENT:
            return {
                ...state,
                counter: state.counter - 1
            };
        case INCREASE_BY_FIVE:
            return {
                ...state,
                counter: state.counter + action.amount
            };
        case SUBTRACT:
            return {
                ...state,
                counter: state.counter - action.amount
            };
        case ADD:
            const newTask = {task: this.state.value, id: this.state.currentId + 1};
            return {
                ...state,
                tasks: state.tasks + action.amount,
                currentId: this.state.currentId+1,
            };
        case REMOVE:
            return {
                ...state,
                counter: state.counter - action.amount
            };
        case FETCH_COUNTER_REQUEST:
            return {
                ...state,
                loading: true
            };
        case FETCH_COUNTER_SUCCESS:
            return {
                ...state,
                loading: false,
                counter: action.counter
            };
        case FETCH_COUNTER_ERROR:
            return {
                ...state,
                loading: false,
            };
        default:
            return state;

    }
};

export default reducer;