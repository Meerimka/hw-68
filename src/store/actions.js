import axios from '../axios-list';

export  const ADD = 'ADD';
export  const REMOVE = 'REMOVE';
export const FETCH_TASKS = 'FETCH_TASKS';
export const FETCH_COUNTER_REQUEST = 'FETCH_COUNTER_REQUEST';
export const FETCH_COUNTER_SUCCESS = 'FETCH_COUNTER_SUCCESS';
export const FETCH_COUNTER_ERROR = 'FETCH_COUNTER_ERROR';

export const INCREMENT = 'INCREMENT';
export const DECREMENT = 'DECREMENT';
export const INCREASE_BY_FIVE = 'INCREASE_BY_FIVE';
export const SUBTRACT  = 'SUBTRACT';


export const fetchTasksSuccess = (data) => {
    return {type: FETCH_TASKS, data};
};

export const fetchTasks = () => {
    return dispatch => {
        return axios.get('/task.json').then(response => {
            dispatch(fetchTasksSuccess(response.data));
        })
    }
};

export const addTask = task =>{
    return dispatch => {
        return axios.post('/task.json', task).then(() => {
            dispatch(fetchTasks())
        })
    }
};
export const removeTask = amount=>{
    return dispatch => {
        dispatch({type: REMOVE, amount});
        dispatch(addCounter());
    }
};

export const incrementCounter = () => {
    return {type: INCREMENT}
};

export const decrementCounter = () => {
    return {type: DECREMENT}
};

export const increaseByFiveCounter = (amount) => {
    return {type: INCREASE_BY_FIVE, amount}
};

export const subtractCounter = (amount) => {
    return {type: SUBTRACT, amount}
};


export const fetchCounterRequest = () =>{
    return {type: FETCH_COUNTER_REQUEST}
};

export const fetchCounterSuccess = counter =>{
    return {type: FETCH_COUNTER_SUCCESS, counter}
};

export const fetchCounterError = error =>{
    return {type: FETCH_COUNTER_ERROR, error}
};

export const fetchCounter = () =>{
    return dispatch =>{
        dispatch(fetchCounterRequest());
        return axios.get('/counter.json').then(response =>{
            dispatch(fetchCounterSuccess(response.data));
        },error =>{
            dispatch(fetchCounterError());
        });
    }
};

export const addCounter = () =>{
    return (dispatch, getState) =>{
        const counter = getState().counter;
        axios.put('/task.json', counter).then(response =>{
            dispatch(fetchCounterSuccess(response.data));
        })
    }
}

export const saveCounter = () => {
    return (dispatch, getState) => {
        const Counter = getState().counter;
        axios.put('/counter.json', Counter)
    }
}