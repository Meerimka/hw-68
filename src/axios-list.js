import axios from 'axios';

const instance =axios.create({
    baseURL: 'https://lb-68-nia.firebaseio.com/'
});

export default instance;