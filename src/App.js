import React, {Component} from 'react';
import './App.css';
import TaskForm from "./components/AddTaskForm/AddTaskForm";
import Task from "./components/Tasks/Tasks";
import {BrowserRouter, Route, Switch} from 'react-router-dom';
import Counter from "./components/Counter/Counter";

class App extends Component {

    render(){
       return (
           <div className="App">
           <BrowserRouter>
               <Switch>
                   <Route path="/form" component={TaskForm} />
                   <Route path="/" exact component={Task} />
                   <Route path='/counter' exact component={Counter}/>
                   <Route render={()=><h1>Not Found!</h1>} />
               </Switch>
           </BrowserRouter>
           </div>

           // <div className="App">
           //     <TaskForm addHandler={this.addHandler} value={this.state.value} changeHandler={this.changeHandler}/>
           //     {this.state.tasks.map((task, key) => <Task remove={()=> this.removeHadler(task.id)} task={task.task} key={key}/>)}
           // </div>
       )

    }
}

export default App;